if(!window.SlickLabs) window.SlickLabs = new Object();
window.onload = function(){
	new SlickLabs.USPManager();
}

SlickLabs.USPManager = function SlickLabs_USPManager(){
	this.init();
	this.rowCount = 0;
	this.lastRowIndex = 0;
}

SlickLabs.USPManager.prototype.constructor = SlickLabs.USPManager;

SlickLabs.USPManager.prototype.init = function(){
	this.addEventListeners();
	this.addRemoveOption();
	this.addLabels();
}

SlickLabs.USPManager.prototype.addEventListeners = function(){
	var me = this;

	var addusp = document.getElementsByClassName('addusp');
	if(addusp != null){
		for(var i=0; i<addusp.length; i++){
			addusp[i].addEventListener('click', function(e){
				e.preventDefault();
				me.addUSP(this);
			}, false);
		}
	}
}



SlickLabs.USPManager.prototype.addLabels = function(){
	var items = document.getElementsByClassName('usp-item');

	for(var i=0; i<items.length; i++){
		var label = document.createElement('label');
		label.innerHTML = i +1;
		
		var inputs = items[i].getElementsByTagName('input');
		for(var j=0; j<inputs.length; j++){
			items[i].insertBefore(label, inputs[j]);
		}
	}
}

SlickLabs.USPManager.prototype.addRemoveOption = function(){
	var me = this;
	var items = document.getElementsByClassName('usp-item');
	if(items.length > 1){
		for(var i=0; i<items.length; i++) {
            var remove = document.createElement('span');
            remove.innerHTML = '&times;';
            remove.classList = 'remove';
            remove.addEventListener('click', function (e) {
                e.preventDefault();
                me.removeUSP(this);
            }, false);
            items[i].appendChild(remove);
        }
	}
}

SlickLabs.USPManager.prototype.checkRemoveOption = function(){
    var items = document.getElementsByClassName('usp-item');

	if(items.length <=1){
		var remove = document.getElementsByClassName('remove');
		for(var i=0; i<remove.length; i++){
			remove[i].parentNode.removeChild(remove[i]);
		}
	}
}

SlickLabs.USPManager.prototype.addUSP = function(elem){
	var me = this;
	var parent = elem.parentNode;
	var row = parent.getElementsByClassName('usp-item');

	if(this.lastRowIndex == 0){
		this.lastRowIndex = row.length;
	}
	var clone = '';

	if(row != null){
		for(var i=0; i<row.length; i++){		
			clone = row[i].cloneNode(true);
		}
	}
	this.lastRowIndex++;
	if(clone != ''){
		var label = clone.getElementsByTagName('label');
		for(var j = 0; j<label.length; label++){
			clone.removeChild(label[j]);
		}
	}


	var input = clone.getElementsByTagName('input');
	

	for(var j=0; j<input.length; j++){
		input[j].value = '';
		input[j].name  = '_usp_option_' + this.lastRowIndex;
		input[j].id = '_usp_option_' + this.lastRowIndex;
		
		var label = document.createElement('label');
		label.innerHTML = this.lastRowIndex;
		clone.insertBefore(label, input[j]);
	}

	
	var span = clone.getElementsByTagName('span');
	for(var j=0; j<span.length; j++){
		clone.removeChild(span[j]);
	}

	var remove = document.createElement('span');
	remove.innerHTML = '&times;';
	remove.classList = 'remove';
	remove.addEventListener('click', function(e){
		e.preventDefault();
		me.removeUSP(this);
	}, false);
	clone.appendChild(remove);

	parent.insertBefore(clone, elem);
}

SlickLabs.USPManager.prototype.removeUSP = function (elem){
	var parent = elem.parentNode;
	var input = parent.getElementsByTagName('input');

	var length = 0;

	if(input != null){
		for(var i=0; i<input.length; i++)
			length = input[i].value.length;
	}

	if(length > 0){
		if(window.confirm('Weet je zeker dat je deze rij wilt verwijderen?'))
			parent.parentNode.removeChild(parent);
	} else {
		parent.parentNode.removeChild(parent);
	}

    this.checkRemoveOption();

}


