<?php

namespace Module\USP\Admin\Metabox;

use Module\USP\Usp\USPManager;
use Slick\Metabox\AbstractMetabox;
use SlickLabs\Form\Form;
use SlickLabs\Form\FormBuilder;	


/**
 * The Metabox class is used to add metabox functionality to
 * the adminpanel posts
 *
 * @author Nathan Jansen <info@nathanjansen.nl>
 * @author Leo Flapper <info@leoflapper.nl>
 * @version 1.0.0
 */
class USPMetabox extends AbstractMetabox
{
	protected $form;

	protected $formElements;

	protected $currentUSPs;


	/**
	 * Contructs the Metabox
	 * @param array $postType   An list of post types that will use this metabox
	 * @param string $context  	The part of the page where the edit screen section should be shown 
	 *                          ('normal', 'advanced', or 'side')
	 * @param string $priority  The priority within the context where the boxes should show 
	 * 	                        ('high', 'core', 'default' or 'low')
	 * @param array  $args      
	 */
	public function __construct($postTypes, $context = 'advanced', $priority='high', $args=[])
	{
		$defaultArgs = [
			'template' => 'usp-metabox',
		];

		$args = array_replace_recursive($defaultArgs, $args);

		$this->setName('USPMetabox');
		$this->setHeadline(__('Unique Selling Points', 'usp-module'));
		$this->setPostTypes($postTypes);
		$this->setViewType($args['template']);
		$this->setContext($context);
		$this->setPriority($priority);
		$this->setForm();
		if($this->isCurrentPostType()){
			if(!$this->isSave()){
				$this->setValues();
			}

 			$this->init();
		}
	}

	public function isCurrentPostType()
	{
		global $pagenow;

		$postType = '';
		if('post.php' === $pagenow) {

			if(isset($_GET['post'])) {
				$postId = $_GET['post'];
			} elseif(isset($_POST['post_ID'])) {
				$postId = $_POST['post_ID'];
			} else {
				return false;
			}

			$postType = get_post_type( $postId );
		} elseif('post-new.php' === $pagenow) {
			if(isset($_GET['post_type'])) {
				$postType = $_GET['post_type'];
			}
		}

		if($postType) {
			if (in_array($postType, $this->getPostTypes())) {
				return true;
			}
		}
		

		return false;
	}



	/**
	 * Get the existing USPs
	 */
	public function setCurrentUSPs()
	{
		$uspManager = new USPManager();
		$this->currentUSPs = $uspManager->getUSP();
	}

	/**
	 * Create the form base
	 */
	public function setForm()
	{
		$formBuilder = new FormBuilder();
		$formBuilder
			->add('_usp_option_1', 'text', '', null);

		$this->form = $formBuilder;
		$this->form->getElement('_usp_option_1')->setBefore('<div class="usp-item">')->setAfter('</div>');
	}
	
	/**
	 * [updateForm description]
	 * @param  string $key   [description]
	 * @return void 	The updated form
	 */
	public function updateForm($key)
	{
		$formBuilder = $this->form;
		$formBuilder
			->add($key, 'text', '', null);

		$this->form = $formBuilder;
	}

	/**
	 * Set's the values of the existing USPs
	 */
	public function setValues()
	{
		if(!isset($_GET['post'])) {
			return false;
		}

		$postID = $_GET['post'];

		$meta = get_post_meta($postID);
		$allowPrepend = '_usp_option_';


		foreach ($meta as $key => $value) {
			if(preg_match('/^' . $allowPrepend . '(\w*)/', $key)){
				if(!$this->form->getElement($key)){
					$this->updateForm($key);
				}

				$this->form->getElement($key)->setValue($value[0])->setBefore('<div class="usp-item">')->setAfter('</div>');
			}
		}



	}

	/**
	 * Get the form of the USP
	 * @return void
	 */
	public function getForm()
	{
		return $this->form->build();

	}


	/**
	 * Check if the post is on saving state
	 * @return boolean 
	 */
	public function isSave(){
		if(isset($_POST['ID'])){
			return true;
		}
		return false;
	}

	/**
	 * Save all the data of the USP form to the database
	 * @param  int $postId The id of the post
	 * @return void
	 */
	public function save($postId)
	{
		if ($this->verifySaveAuthorization($postId)) {
			return $postId;
		}

		$uspState = [];
		$allowPrepend = '_usp_option_';
		$count = 1;

        $this->deleteAllUspItemsByPostId($postId);

        //This will save the meta to the right place, only get the USP meta by the prepend
		foreach($_POST as $key => $value){
			if(preg_match('/^' . $allowPrepend . '(\w*)/', $key)){
                $newName = $allowPrepend . $count;

                if (isset($_POST[$key]) && (string) $_POST[$key] !== ''){
                    update_post_meta($postId, $newName, $_POST[$key]);
                }

				$count++;
			}
		}
	}

    /**
     * If the Post metadata not exists in the $_POST, it will be deleted
     * @param $postId
     * @return void
     */
    public function deleteAllUspItemsByPostId($postId)
    {
        global $wpdb;
        $meta = $wpdb->get_results(
        "SELECT meta_key 
          FROM {$wpdb->prefix}postmeta 
          WHERE meta_key LIKE '_usp_option_%'
          AND post_id={$postId}
        ", ARRAY_N
        );

        foreach ($meta as $key => $value) {
            foreach($value as $item => $val) {
                delete_post_meta($postId, $val);
            }
        }
    }
}