<?php

namespace Module\USP\Admin;

use Slick\AbstractBootstrap;
use Slick\Application;
use Slick\Metabox\Metabox;
use Module\USP\Bootstrap as FrontendBootstrap;
use Module\USP\Admin\Metabox\USPMetabox;
use Slick\Script\Script;


/**
 * The admin bootstrap
 *
 * @author Niels van der Schaaf
 * @version 1.0.0
 */
class Bootstrap extends AbstractBootstrap
{
    public function _initScripts()
    {
        if (!defined('SLICKLABS_VERSION')) {
            add_action('admin_head', [$this, 'includeCSS']);
            add_action('admin_head', [$this, 'includeJS']);

            return;
        }

        // Register admin styles and scripts here
        Application::getScriptManager()->add(new Script(
            'uspAdmin',
            Script::TYPE_JAVASCRIPT,
            FrontendBootstrap::getStaticModule()->getPath() . '/assets/js/USPManager.js',
            true
        ));

        // Register admin styles and scripts here
        Application::getScriptManager()->add(new Script(
            'uspAdmin',
            Script::TYPE_STYLE,
            FrontendBootstrap::getStaticModule()->getPath() . '/assets/css/uspStyle.css',
            true
        ));
   }

    /**
     * Add the Javascript to the head of the Dashboard
     * @return void
     */
    public function includeJS()
    {
        wp_enqueue_script('usp-admin-js', FrontendBootstrap::getStaticModule()->getUrl() . '/assets/js/USPManager.js');
    }

    /**
     * Add the CSS to the head of the Dashboard
     * @return void
     */
    public function includeCss()
    {
        wp_enqueue_style('usp-admin-style', FrontendBootstrap::getStaticModule()->getUrl() . '/assets/css/uspStyle.css');
    }

    /**
     * Initializes the custom metabox for the USPs
     * @return Metabox
     */
    public function _initMetabox()
    {
        new USPMetabox('usp');
    }
}
