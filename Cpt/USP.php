<?php 

namespace Module\USP\Cpt;

/**
 *  The USP module
 *
 * @author Niels van der Schaaf <niels.schaaf@slicklabs.nl>
 * @version 0.0.1 Alpha
 */

class USP 
{
	protected $title = 'usp';

	/**
	 * initializes the custom post type
	 * @return void
	 */
	public function init()
	{
		add_action('init', [$this, 'register']);
	}


	/**
	 * Register the custom post type
	 * @return void
	 */
	public function register()
	{
		register_post_type($this->getTitle(), [
			'labels' => [
				'name'               => __('USP', 'usp-module'),
                'singular_name'      => __('USP', 'usp-module'),
                'add_new'            => __('Nieuwe USP', 'usp-module'),
                'add_new_item'       => __('Nieuwe USP', 'usp-module'),
                'edit_item'          => __('USP bewerken', 'usp-module'),
                'new_item'           => __('Nieuwe USP', 'usp-module'),
                'view_item'          => __('USP bekijken', 'usp-module'),
                'search_items'       => __('USP zoeken', 'usp-module'),
                'not_found'          => __('Geen USP gevonden', 'usp-module'),
                'not_found_in_trash' => __('Geen USP gevonden', 'usp-module'),
                'menu_name'          => __('USP', 'usp-module')
			],
			'hierarchical' => false,
			'public' => false,
			'show_ui' => true,
			'menu_icon' => 'dashicons-index-card',
			'supports' => ['title']
		]);
	}

	/**
	 * Get the title of the custom post type
	 * @return string 
	 */
	public function getTitle()
	{
		return $this->title;
	}
}