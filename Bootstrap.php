<?php

namespace Module\USP;

use Module\USP\Admin\Bootstrap as AdminBootstrap;
use Module\USP\Cpt\USP;
use Slick\AbstractBootstrap;
use Slick\Module\AutoloadableInterface;
use Slick\Module\ModuleAwareInterface;
use Slick\Module\ModuleInfo;

/**
 * The USP bootstrap
 *
 * @author Niels van der Schaaf <niels.schaaf@slicklabs.nl>
 * @version 1.0.0
 */
class Bootstrap extends AbstractBootstrap implements AutoloadableInterface, ModuleAwareInterface
{

	protected static $moduleInfo;

    /**
     * Register this module with the Standard PHP Library (SPL).
     */
    public function getAutoloaderConfig()
    {
        return [
            'Spark\Loader\StandardAutoloader' => [
                'namespaces' => [
                    'Module\USP' => __DIR__,
                ],
            ],
        ];
    }

    /**
     * Register the module
     * @param ModuleInfo $module
     */
    public function setModule(ModuleInfo $module)
    {
    	return self::$moduleInfo = $module;
    }

    /**
     * Initializes the USP custom post type
     * @return void
     */
    public function _initUSP()
    {
        $usp = new USP();
        $usp->init();
    }

    /**
     * Initializes the admin bootstrap on admin init
     * @return void
     */
    public function _initAdmin() 
    {
        $adminBootstrap = new AdminBootstrap();
        add_action('admin_init', [$adminBootstrap, 'init']);
    }

    /**
     * Store the module
     * @return void
     */
    public static function getStaticModule()
    {
    	return self::$moduleInfo;
    }


}