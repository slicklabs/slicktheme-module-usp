The USO module allows the user to add Unique Selling Points to it's website.


# Installation via Composer #
1. Make sure the repository is added to the 'composer.json' file. Add the following code to make sure composer can load from the repository.
```
#!php
"repositories": [
  {
      "type": "git",
      "url": "https://bitbucket.org/slicklabs/slicktheme-module-usp.git",
      "branches-path": "master"
  }
]
```

2. Add the following requirement to the composer.json
```
#!php
"require": {
    "slicklabs/slicktheme/module/USP": "@dev"
}
```

3. Run composer update from the terminal

# Installation via GIT #
1. Clone this repository into the theme 'modules' folder by the following command: 
```
#!

git clone https://slicklabs@bitbucket.org/slicklabs/slicktheme-module-usp.git USP
```

2. Make sure the module directory name is 'USP'


# USPs Admin Page #

When installed and activated a USPs Admin Page appears in the backend settings menu of Wordpress.
There you are able to create a USP and fill in the information. 

# Helpers #
For retrieving the locations data you have to use the helper class provided by the module

# HTML list #

There is also default template available which returns a list of available locations with the information. 
```
#!

<?php echo $this->helper->USPHelper->list(); ?>
```
