<?php 

namespace Module\USP\Helper;

use Slick\View;

class USP
{

	/**
	 * Get the meta of the USP
	 * @param  int $postId The post id
	 * @return array Contains all the meta data of the usp
	 */
	public function getMeta($postId)
	{
		$meta = get_post_meta($postId);
		$allowedPrepend = '_usp_option_';
		$result  = [];

		foreach ($meta as $key => $value) {
            preg_match('/^' . $allowedPrepend . '(\w*)/', $key, $matches);
            if (isset($matches[0])) {
                $result[$matches[1]] = $value[0];
            }
            
        }

       return $result;
	}

	/**
	 * Create's a list of the usps
	 * @param  array  $args to specifie more details
	 * @return view
	 */
	public function show(array $args = [])
    {
        $defaultArgs = [
          'template' => 'templates/partials/usp/list.phtml',
            'queryArgs' => [
                'post_type' => 'usp',
                'orderby' => 'menu_order'
            ],
            'args' => [],
        ];


        $args = array_replace_recursive($defaultArgs, $args);

        $view = new View();
        query_posts($args['queryArgs']);

        $output = '';
        if (have_posts()) {
            $output = $view->partial($args['template'], $args['args']);
        }

        wp_reset_query();

        return $output;
    }

}